Package.describe({
  name: "hsed:impersonate",
  summary: "Impersonate users in Meteor",
  version: "1.0.0",
  git: "https://bitbucket.org/hsedevelopment/meteor-impersonate",
});

Package.onUse(function (api, where) {
  api.versionsFrom("2.5");
  api.use("ecmascript");
  api.use([
    "accounts-base",
    "reactive-var",
    "templating",
    "tracker",
    "jquery",
    "hsed:body-events@1.0.0",
  ], "client");
  api.use([
    "alanning:roles@2.2.0 || 3.4.0",
  ], "server");
  api.mainModule("client/lib.js", "client");
  api.mainModule("server/lib.js", "server");
});
