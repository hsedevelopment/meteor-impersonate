import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { ReactiveVar } from 'meteor/reactive-var';
import { Template } from 'meteor/templating';
import { $ } from 'meteor/jquery';

export const Impersonate = {
  _user: null,
  _token: null,
  _active: new ReactiveVar(false),
  _waiting: new ReactiveVar(false),
  isActive() {
    return this._active.get();
  },
  isWaiting() {
    return this._waiting.get();
  },
  do(toUser, cb) {
    const params = { toUser: toUser };
    if (Impersonate.isWaiting()) {
      throw new Error('Impersonating is in progress');
    }
    if (this._user) {
      params.fromUser = this._user;
      params.token = this._token;
    }
    Meteor.call("impersonate", params, (err, res) => {
      Impersonate._waiting.set(false);
      if (err) {
        console.log("Could not impersonate.", err);
        if (cb && cb.constructor && cb.apply) cb(err);
      } else {
        if (!this._user) {
          this._user = res.fromUser; // First impersonation
          this._token = res.token;
        }
        this._active.set(true);
        Meteor.connection.setUserId(res.toUser);
        if (cb && cb.constructor && cb.apply) cb(null, res.toUser);
      }
    });
    Impersonate._waiting.set(true);
  },
  undo(cb) {
    this.do(this._user, (err, res) => {
      if (err) {
        console.log("Could not unimpersonate.", err);
        if (cb && cb.constructor && cb.apply) cb(err);
      } else {
        this._active.set(false);
        if (cb && cb.constructor && cb.apply) cb(null, res.toUser);
      }
    });
  },
};

// Reset data on logout
Tracker.autorun(function() {
  if (Meteor.userId()) return;
  Impersonate._active.set(false);
  Impersonate._user = null;
  Impersonate._token = null;
});

Template.body.events({
  "click [data-impersonate]": function(e, data) {
    const userId = $(e.currentTarget).attr("data-impersonate");
    Impersonate.do(userId);
  },
  "click [data-unimpersonate]": function(e, data) {
    Impersonate.undo();
  }
});

Template.registerHelper("isImpersonateActive", function () {
  return Impersonate.isActive();
});

Template.registerHelper("isImpersonateWaiting", function () {
  return Impersonate.isWaiting();
});
